#include "universal/data/csv.hpp"

#include <sstream>
#include <vector>

using namespace universal::data::csv;

Lecteur::Lecteur( Lecteur::stream_t * flux, Dialecte d ) :
	_flux( flux ),
	dialecte( std::move( d ) ) {
}

Lecteur_iterator Lecteur::begin() {
	this->_flux->seekg( 0 );
	return { this->_flux, this->dialecte };
}

//
//
//

Lecteur_iterator::Lecteur_iterator( Lecteur_iterator::stream_t * _in, Dialecte dialecte ) :
	_in( _in ),
	dialecte( std::move( dialecte ) ) {
	lecture();
}

bool Lecteur_iterator::operator==( Lecteur_iterator const & cl ) const noexcept {
	return ( _in == nullptr && cl._in == nullptr ) || ( _in == cl._in && ligne() == cl.ligne() );
}

Lecteur_iterator & Lecteur_iterator::operator++() {
	lecture();

	// Dernière ligne vide
	if( this->_valeurs.empty() ) {
		terminaison();
	} else
		++_ligne;

	return *this;
}

std::vector<Lecteur_iterator::string_t> Lecteur_iterator::operator*() && {
	return std::move( _valeurs );
}

bool Lecteur_iterator::fail() const noexcept {
	return this->_in != nullptr && this->_in->fail();
}

void Lecteur_iterator::lecture() {
	this->_valeurs.clear();
	this->_valeurs.reserve( 12 );
	std::basic_stringbuf<value_t> ss;

	while( !this->_in->fail() && char_trait::not_eof( this->_in->peek() ) ) {
		auto c = this->_in->get();
		if( c == '\r' ) continue;
		if( c == this->dialecte.sep || c == '\n' ) {
			this->_valeurs.emplace_back( std::move( ss ).str() );
			ss.str( "" );
			// Fin de ligne
			if( c == '\n' ) return;
		} else if( c == '"' ) {
			while( !this->_in->fail() && !char_trait::not_eof( this->_in->peek() ) && c != '"' ) {
				c = this->_in->get();
				if( c == '"' )
					break;
				else
					ss.sputc( (value_t)c );
			}
		} else
			ss.sputc( (value_t)c );
	}

	if( _in->fail() ) {
		terminaison();
		return;
	}

	if( !_in->fail() && char_trait::eof() == _in->peek() && ss.in_avail() > 0 ) {
		_valeurs.emplace_back( ss.str() );
	}
}

void Lecteur_iterator::terminaison() {
	_in = nullptr;
	_valeurs.clear();
}
