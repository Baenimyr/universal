#pragma once

#include "universal/data/api.h"

#include <cstddef>
#include <istream>
#include <string>
#include <vector>

namespace universal::data::csv {

struct UNIVERSAL_DATA_API Dialecte {
	char sep;

	constexpr Dialecte( char sep = ',' ) :
		sep( sep ) {
	}
};	//// Dialecte

class Lecteur;
class Lecteur_iterator;

/** @brief Itérateur de lecture d'un fichier CSV.
 *
 * Une nouvelle ligne est lue pour chaque appel à \code ++l \endcode.
 *
 * Le texte est recopié dans des std::string après avoir retirer les caractères spéciaux et
 * échappés.
 */
class UNIVERSAL_DATA_API Lecteur {
public:
	using value_t = char;
	using char_trait = std::char_traits<value_t>;
	using stream_t = std::basic_istream<value_t, char_trait>;

private:
	stream_t * _flux;

public:
	Dialecte dialecte;

public:
	Lecteur( stream_t * flux, Dialecte d = {} );
	Lecteur( Lecteur const & ) noexcept = default;
	Lecteur( Lecteur && ) noexcept = default;

	Lecteur & operator=( Lecteur const & ) noexcept = default;
	Lecteur & operator=( Lecteur && ) noexcept = default;

	/** Réinitialise la position de lecture et retourne un itérateur. */
	Lecteur_iterator begin();

	/** Créer un itérateur sans flux associé. */
	inline constexpr Lecteur_iterator end() const noexcept;
};	//// Lecteur

class UNIVERSAL_DATA_API Lecteur_iterator {
public:
	using value_t = Lecteur::value_t;
	using char_trait = Lecteur::char_trait;
	using string_t = std::basic_string<value_t, char_trait>;
	using stream_t = Lecteur::stream_t;

	friend class Lecteur;

private:
	stream_t * _in = nullptr;
	Dialecte const dialecte;
	size_t _ligne = 0;
	std::vector<string_t> _valeurs;

public:	 // -- Constructeurs
	/** @brief Crée un itérateur sans flux associé. Correspond à end(). */
	constexpr Lecteur_iterator() = default;

	/** @brief Crée un itérateur sur un flux et un dialecte. */
	Lecteur_iterator( stream_t *, Dialecte = {} );

	constexpr ~Lecteur_iterator() noexcept = default;

	Lecteur_iterator( Lecteur_iterator && ) = delete;
	Lecteur_iterator( Lecteur_iterator const & ) = delete;

	Lecteur_iterator & operator=( Lecteur_iterator && ) = delete;
	Lecteur_iterator & operator=( Lecteur_iterator const & ) = delete;

public:
	inline size_t ligne() const noexcept;

	bool fail() const noexcept;

	inline bool end() const noexcept;

	bool operator==( Lecteur_iterator const & ) const noexcept;

	inline std::vector<string_t> const & operator*() const &;
	std::vector<string_t> operator*() &&;

	inline std::vector<string_t> const * operator->() const;

	Lecteur_iterator & operator++();

private:
	void lecture();
	void terminaison();
};	//// Lecteur

//
// Inlines
//
constexpr Lecteur_iterator Lecteur::end() const noexcept {
	return {};
}

size_t Lecteur_iterator::ligne() const noexcept {
	return _ligne;
}

bool Lecteur_iterator::end() const noexcept {
	return _in == nullptr;
}

std::vector<Lecteur_iterator::string_t> const & Lecteur_iterator::operator*() const & {
	return _valeurs;
}

std::vector<Lecteur_iterator::string_t> const * Lecteur_iterator::operator->() const {
	return &_valeurs;
}
}  // namespace universal::data::csv
