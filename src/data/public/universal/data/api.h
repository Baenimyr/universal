#ifndef universal_data_SHARED
	#define universal_data_SHARED 0
#endif

#ifndef UNIVERSAL_DATA_API
	#if !universal_data_SHARED
		#define UNIVERSAL_DATA_EXPORT
		#define UNIVERSAL_DATA_IMPORT
		#define UNIVERSAL_DATA_NO_EXPORT
	#elif __linux__
		#define UNIVERSAL_DATA_EXPORT __attribute__( ( visibility( "default" ) ) )
		#define UNIVERSAL_DATA_IMPORT __attribute__( ( visibility( "default" ) ) )
		#define UNIVERSAL_DATA_NO_EXPORT __attribute__( ( visibility( "hidden" ) ) )
	#elif _WIN32
		#define UNIVERSAL_DATA_EXPORT __declspec( dllexport )
		#define UNIVERSAL_DATA_IMPORT __declspec( dllimport )
		#define UNIVERSAL_DATA_NO_EXPORT
	#else
		#error "Platform not supported"
	#endif
#endif

#if universal_data_EXPORTS
	#define UNIVERSAL_DATA_API UNIVERSAL_DATA_EXPORT
#else
	#define UNIVERSAL_DATA_API UNIVERSAL_DATA_IMPORT
#endif
#define UNIVERSAL_DATA_LOCAL UNIVERSAL_DATA_NO_EXPORT

#ifndef UNIVERSAL_DATA_DEPRECATED
	#define UNIVERSAL_DATA_DEPRECATED __attribute__( ( __deprecated__ ) )
#endif
