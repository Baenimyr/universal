#include "universal/data/csv.hpp"

#include <gtest/gtest.h>
#include <iterator>
#include <sstream>

using namespace universal::data::csv;

TEST( CSV, lecture ) {
	char data []
		= "tconst	\"titleType\"	primaryTitle	\"original\"\"Title\"	isAdult	startYear	"
		  "endYear	runtimeMinutes	genres\n";

	std::istringstream f( data );
	Lecteur csv( &f, Dialecte( '\t' ) );
	auto l = std::begin( csv );
	for( ; l != std::end( csv ); ++l ) {
		if( l.ligne() == 0 ) {
			auto const & data = *l;
			ASSERT_EQ( 9, data.size() );
			EXPECT_EQ( "tconst", data.at( 0 ) );
			EXPECT_EQ( "titleType", data.at( 1 ) );
			EXPECT_EQ( "primaryTitle", data.at( 2 ) );
			EXPECT_EQ( "originalTitle", data.at( 3 ) );
		}
	}
	EXPECT_EQ( 0, l.ligne() );
	EXPECT_TRUE( l.end() );
}
